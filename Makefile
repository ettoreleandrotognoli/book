
CONTAINER?=docker
PANDOC_IMAGE?=pandoc/latex
PLANTUML_IMAGE?=miy4/plantuml
USER=$(shell id --user):$(shell id --group)
PANDOC_CONFIG=src/main/resources/pandoc.yml
PANDOC_METADATA=src/main/resources/metadata.yml
PLANTUML_METADATA=src/main/resources/plantuml.config
CONTAINER_PARAMS=--rm --user ${USER} -w ${PWD} -v ${PWD}:${PWD}
PANDOC=${CONTAINER} run ${CONTAINER_PARAMS}  ${PANDOC_IMAGE} -d ${PANDOC_CONFIG} --toc --webtex --metadata-file ${PANDOC_METADATA}
PLANTUML=${CONTAINER} run ${CONTAINER_PARAMS} ${PLANTUML_IMAGE} -charset UTF-8 -config src/main/resources/plantuml.config
HTML_ARGS=--standalone --self-contained --to html5
DOCX_ARGS=--standalone
PLANTUML_PNG=$(patsubst src/main/plantuml/%,target/resources/%,$(patsubst %.puml,%.png,$(shell find src/main/plantuml/ -name "*.puml")))
PLANTUML_SVG=$(patsubst src/main/plantuml/%,target/resources/%,$(patsubst %.puml,%.svg,$(shell find src/main/plantuml/ -name "*.puml")))

OUTPUT=book

SOURCES=$(shell find src/main/markdown -xtype f -print0 | sort -z| xargs -r0 echo)

RESOURCES=$(shell find src/main/resources -xtype f)

DEPENDENCIES=Makefile target ${SOURCES} ${RESOURCES}

all: pdf epub html docx

pdf: target/${OUTPUT}.pdf

epub: target/${OUTPUT}.epub

html: target/${OUTPUT}.html

docx: target/${OUTPUT}.docx


target:
	mkdir -p target


target/%.html: ${DEPENDENCIES}  ${PLANTUML_SVG}
	${PANDOC} ${HTML_ARGS} -o $@ ${SOURCES}

target/%.pdf:  ${DEPENDENCIES} ${PLANTUML_PNG}
	${PANDOC} --pdf-engine pdflatex --default-image-extension=".png" -o $@ ${SOURCES}

target/%.epub: ${DEPENDENCIES} ${PLANTUML_SVG}
	${PANDOC} -o $@ ${SOURCES}

target/%.docx: ${DEPENDENCIES}  ${PLANTUML_SVG}
	${PANDOC} ${DOCX_ARGS} -o $@ ${SOURCES}



target/resources/%.png: src/main/plantuml/%.puml ${PLANTUML_METADATA}
	${PLANTUML} $^ -tpng -o ${PWD}/$(shell dirname $@)


target/resources/%.svg: src/main/plantuml/%.puml ${PLANTUML_METADATA}
	${PLANTUML} $^ -tsvg -o ${PWD}/$(shell dirname $@)

clean:
	rm -rf target/
	rm -rf ${PLANTUML_SVG} ${PLANTUML_PNG}